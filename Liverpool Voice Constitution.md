# Liverpool Voice

## Constitution

**1. Name:**

The choir shall be known as Liverpool Voice (hereinafter referred to as
“the Choir” or, alternatively, “the association”).

**2. Aims & Objectives:**

- a. To promote & encourage choral singing for the membership of the association.

- b. To support and develop the skills of the members for the betterment of their wellbeing and contribute to the life of the community.

- c. To offer public performances and recitals of choral music and any other collaborative works which the Artistic Director, in consultation with the members, may decide upon.

- d. Liverpool Voice exists as a non-profitmaking, unincorporated association.

**3. Equal Opportunities:**

Liverpool Voice is an inclusive organisation. No individual shall be excluded from membership or de-barred from any official capacity on the grounds of sex, race, colour, age, religion, sexual orientation, disability or political affiliation.

**4. Membership:**

- a. Membership is open to any person 18 years and over (or at the discretion of the Artistic Director), who is interested in singing and who will abide by this constitution.

- b. Applicants must fill out an application form, available from the Secretary AND

- c. Be auditioned, as deemed appropriate, by the Artistic Director, whose decision on their suitability is final and binding.

- d. The Artistic Director, together with the Committee, may from time to time declare the maximum number in each voice section, in the interest of balance, and if that maximum number is reached, its membership shall be closed and a waiting list shall be opened.

- e. The waiting list shall be reviewed from time to time as deemed appropriate by the Artistic Director and any vacancies arising shall be filled from the waiting list first, after an audition.

- f. Rehearsals shall take place 18:45-21:00 once a week between September and July, or as the Artistic Director shall from time to time request, according to musical commitments.

- g. If a member is unable to attend a particular rehearsal, they must notify the Artistic Director before the rehearsal.

- h. Musical scores loaned to a member may be annotated lightly IN PENCIL, shall be cared for by the member and must be returned to the association upon request. Such scores will be saved as part of the repertoire of the choir for future use, thus creating a choral library. Whether scores are owned or hired by the association, due care must be taken not to damage the music.

- i. Any member whose attendance or behaviour is considered to be inappropriate or to bring the choir into disrepute, or fails to abide by this constitution, may have their membership closed by the Committee.

- j. It shall be the prerogative of the Committee to nominate, as an Honorary Life Member, any person who has rendered exceptional service to the Choir, or to Music or the Arts in that locality, and whose distinguished position would be considered an honour to the Association. Such a nomination shall be proposed and ratified at the AGM.

**5. Administration and Management**

- a. The organisation of the affairs of the association shall be vested in a Committee, to be elected at each AGM of the association, by ballot. An AGM shall be held usually in September but not less than once in each calendar year. A notice of an AGM shall be circulated at least 21 days beforehand with details of location, time and agenda, which will in all cases include:

    - i. the receipt of accounts

    - ii. the election of officers for the forthcoming year

    - iii. Any other business which may be properly dealt with at an AGM.

- b. The Committee shall consist of the Chair, Treasurer and Secretary, ex-officio member(s), together with a representative from each of the voice sections (Soprano, Alto, Tenor, Bass, [SATB]). A person may represent a voice section in addition to another role on the Committee.

No less than 3 Committee members present at a Committee meeting shall constitute a quorum.

- c. Nominations for officers and Committee members, with the consent of the nominee, should be duly proposed and seconded in writing and forwarded to the Secretary following receipt of the notice of the AGM. They may also be presented at the time of the AGM.

If necessary, a ballot will be taken.

No less than 30% of the membership present at a general meeting shall constitute a quorum.
- d. Vacancies on the Committee shall arise when the Secretary has received written notification of resignation from a serving member.
- e. In the event of a vacancy arising, the Committee may co-opt a member to fill the vacancy until the next General Meeting, where the vacancy shall be filled by election in the normal way. Notice of a General Meeting shall be circulated 14 days beforehand, with details of location, time and agenda. Nominations and election shall be made as in c., above.
- f. The Artistic Director shall be an ex-officio member of the Committee and shall be duly notified of all Committee meetings. The Artistic Director may audition members and prospective members of the Association as deemed appropriate. The Artistic Director shall propose a plan of events for the year ahead, taking into consideration the progress of the choir, numbers singing and the wishes of the majority of the choir’s members. The Artistic Director shall advise the Committee and the Association on the suitability of selected pieces to be rehearsed and performed. The Artistic Director shall have complete authority at rehearsals and performances in all matters relating to musical standards and levels of proficiency.
- g. An accompanist shall be appointed by the Committee in consultation with the Artistic Director. The accompanist’s fees and expenses shall be agreed by the Committee.
    - The Accompanist shall liaise closely with the Artistic Director.
- h. An Annual Members’ Subscription shall be decided at the AGM and shall be operational until the next AGM. The Committee may, at its absolute discretion, approve concessions to an individual’s subscription without affecting the status of such member’s membership or entitlement to vote.
- i. Subject to h., above, only members fully paid up before the AGM are entitled to vote at the AGM.
- j. The proceedings of each AGM, General Meeting and Committee meetings are to be recorded by the Secretary or a nominated Committee member. Such minutes, signed and dated by the Chairperson, shall in the absence of proof of error, be accepted as evidence of the facts therein stated.
- k. The Committee may, at any time, appoint a sub-Committee or co-opt an individual member or members to assist in the operation of the affairs of the association. Any such sub-Committee or member shall be responsible to the Committee.
- l. The Committee shall be responsible to members regarding the administration of the association.
- m. Committee members shall hold office for one year and may offer themselves for re-election annually, at least 28 days before the AGM.

**6. Duties of Officers:**

- a. The Chair shall be responsible for the leadership of the association, coordinating its administration and shall chair meetings. In the absence of the Chair, another committee member may be designated to assume the office of Chair.
- b. The Secretary shall be responsible for ensuring that administrative arrangements are put into effect and for the day-to-day running of the association.
- c. The Treasurer shall be responsible for financial control, keeping the accounts in order, ensuring that a financial statement is provided to the Committee as required and reporting at Annual General Meetings.

**7. Finance**

The sources of income for the Choir shall include but shall not be limited to:
- i. Annual Membership Subscriptions at a rate to be fixed at the AGM
- ii. Private donations
- iii. Revenue from performances
- iv. Grants or sponsorship that may be recommended by the Committee
- v. Prize money from choral competitions.

The assets of the Choir shall be held by the Committee, who will maintain a bank account in the name of the Choir, under the control of two signatories who shall be Committee members. All expenditure shall be agreed in advance, ideally by the Committee but, in all cases, in consultation with the Treasurer. All money raised by the Choir will be spent solely on the objectives laid out in 2., above.

**8. Dissolution**

- a. If the Committee, by a simple majority, decides at a Committee meeting that it is necessary to close down the association, it shall call a Special General Meeting whose sole business shall be to dissolve the association. Notice shall be circulated in writing to all members at least 21 days in advance and shall include the proposal to dissolve and an invitation for proposals of a suitable organisation to receive the remaining assets, if any.

- b. The decision to dissolve the association shall be made by simple majority of those present and entitled to vote.

- c. In the event of the dissolution of the association, the assets shall be realised, and after outstanding debts have been paid, the balance shall be donated to a suitable local organisation, to be agreed at the meeting which approves the dissolution.

- d. In the event of the dissolution of the association, where the debts outweigh the assets, each member of the association shall be responsible for an equal amount of the debts.

**9. Constitutional Amendments**

- a. Alterations to this Constitution may be made at any General Meeting at which 51% of the members are present and will require the approval of a two-thirds majority of those present and entitled to vote. Notice and details of any such proposal must be given in writing to the Secretary at least 4 weeks before the General Meeting.

- b. The proposed alteration shall be circulated in writing to all members, together with the notice convening the General Meeting, no less than 14 days before the date of the meeting.

A ballot shall be taken on the motion in the normal way.
